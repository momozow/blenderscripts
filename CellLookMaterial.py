import bpy

# Create a new material
material = bpy.data.materials.new(name="New_Material")
material.use_nodes = True
nodes = material.node_tree.nodes

# Clear existing nodes
for node in nodes:
    nodes.remove(node)

# Add nodes to the material
output_node = nodes.new(type='ShaderNodeOutputMaterial')
diffuse_node = nodes.new(type='ShaderNodeBsdfDiffuse')
shader_to_rgb_node = nodes.new(type='ShaderNodeShaderToRGB')
color_ramp_node = nodes.new(type='ShaderNodeValToRGB')
color_ramp_node.color_ramp.interpolation = 'CONSTANT'

# Set positions of the nodes
output_node.location = (340, 0)
diffuse_node.location = (-340, 0)
shader_to_rgb_node.location = (-170, 0)
color_ramp_node.location = (0, 0)

# Connect the nodes
links = material.node_tree.links
link1 = links.new(diffuse_node.outputs[0], shader_to_rgb_node.inputs[0])
link2 = links.new(shader_to_rgb_node.outputs[0], color_ramp_node.inputs[0])
link3 = links.new(color_ramp_node.outputs[0], output_node.inputs[0])

# Get currently selected objects
selected_objects = bpy.context.selected_objects

# Apply the material to all selected objects
for obj in selected_objects:
    # Apply the material only if the object is a mesh
    if obj.type == 'MESH':
        # Add a new material slot if the object doesn't have one
        if len(obj.data.materials) == 0:
            obj.data.materials.append(material)
        else:
            obj.data.materials[0] = material
